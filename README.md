Thank you for your visiting my repogitory for publishing my private desktop application.

  (1) Kindly, please download folder of "Bulletine-board-app-win32-x64.7z" and unzip it with 7-Zip tool, which all of unzipped files are to be put into just one folder. 

  (2) Then, please find a file "Bulletine-board-app.exe" and double-click it which is kept still in the folder because of
        some importance of relationship between all those files in kept in one folder, for you to start the application. 
 
  (3) In case using this application with MongoDB, an internet connection is required. In case using only Local Storage Database, this application is available even without an internet connection.   

  (4) When running the application under a contrast theme in the personal settings of Windows display, the CSS may not display correctly with the background color, background image, and text color, but the operation and display of text itself work correctly. Therefore, we recommend to use this application with normal Windows display theme settings, not contrast theme.

  (5) This bulletine board has database in Local Storage in application browser of Chromium, which is with capacity of upto 5MB around due to your using browser and desktop computor, 20 to 50 threads concerning to volume of description. If you need further more volume for database, you can utilize MongoDB with your own MongoDB Atlas account and URI. 

  (6) The contents of data in the local storage as well as the data in MongoDB aren't reset when the app is closed and continues to the next launch of the app.        

  (7) Further for your reference, please download and read Operation Manual PDF file for bulletine board app. If you prefer English language, please select 'English' in the title of PDF file, or Japanese which you refer to '日本語’ title of the PDF file.

With best regards again for your kind interest,



ご訪問有難うございます。私個人作成のアプリを公開しているサイトです。

(1) "Bulletine-board-app-win32-x64.7z"をダウンロードして、7-ZIPにて解凍下さい。解凍後全ファイルを一つのフォルダに纏めて下さい。

(2) 解凍後のファイル一つ一つはそれぞれ依存関係があり、一つのフォルダに纏まっていなければ実効性を失います。"Bulletine-board-app.exe"を見つけてフォルダ内からダブルクリックしてアプリを起動します。

(3) MongoDBを利用の場合はインターネット環境不必要です。アプリブラウザ内のLocal Storage Databaseのみ利用の場合はインターネット環境が無くても動作します。

(4) お使いのパソコンディスプレイ画面表示がコントラスト設定の場合はアプリCSSによる表示が正しく出来ない事があり、画面表示設定はノーマルの状態でアプリをご使用頂きたく存じます。

(5) アプリブラウザにはChromiumが使用されていまして、Local Storageの容量は５MB程度ですので、スレッドにして20から50スレッド程度が目安です。それ以上のデータ容量が必要な場合はMongoDBを利用下さい。その際はご自身のMongoDB AtlasのアカウントとURIをお持ち下さい。MongoDBには無料版もあります。

(6) アプリを一反停止しても、再度利用開始の際には、MongoDBでもLocal Storageでもデータベースは継続します。

(7) 利用ガイドとして、オペレーションマニュアルをダウンロードしてご覧下さい。日本語版と英語版があります。

どうぞ宜しくお願い致します。

　

